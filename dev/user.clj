(ns user
  "Tools for interactive development with the REPL. This file should
  not be included in a production build of the application."
  (:require
   [clojure.java.io :as io]
   [clojure.java.javadoc :refer [javadoc]]
   [clojure.pprint :refer [pprint]]
   [clojure.reflect :refer [reflect]]
   [clojure.repl :refer [apropos dir doc find-doc pst source]]
   [clojure.set :as set]
   [clojure.string :as string]
   [clojure.test :as test]
   [clojure.tools.namespace.repl :refer [refresh refresh-all]]
   [com.stuartsierra.component :as component]
   [immutant.web :as web]
   [diag-server :as app]))

(def serv
  {:name "Server"
   :db {:username "blah"
        :password ""
        :server-name "localhost"
        :port-number "5432"
        :database-name "diag"
        :adapter "postgresql"
        :pool-size 9}
   :port 4000})

(def test
  {:name "Test Referer Server"
   :db {:username "ubuntu"
        :password "tim"
        :server-name "localhost"
        :port-number "5432"
        :database-name "diag"
        :adapter "postgresql"
        :pool-size 9}
   :port 8080})

(def system
  "A Var containing an object representing the application under
  development."
  nil)

(defn init
  "Creates and initializes the system under development in the Var
  #'system."
  []
  (alter-var-root #'system
                  (constantly (app/system test)))
  )

(defn start
  "Starts the system running, updates the Var #'system."
  []
  (alter-var-root #'system component/start))

(defn stop
  "Stops the system if it is currently running, updates the Var
  #'system."
  []
  (alter-var-root #'system
                  (fn [s] (when s (component/stop s))))
  )

(defn go
  "Initializes and starts the system running."
  []
  (init)
  (start)
  :ready)

(defn reset
  "Stops the system, reloads modified source files, and restarts it."
  []
  (stop)
  (refresh :after 'user/go))
