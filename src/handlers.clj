(ns handlers
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]))

(defn query
  "run a raw sql query"
  [db q & params]
  (jdbc/query (:conn db) (cons q params)))

(defn insert!
  "table should be a clojure keyword
  params is a map of keys->values for the db
  calling functions should validate input values
  returns the created value or nil if insert fails"
  [db table params]
  (try (first (jdbc/insert! (:conn db) table
                            params))
       (catch org.postgresql.util.PSQLException e
         (println e)
         nil)))

(defn record-referer
  [db request referer sub]
  (insert! db :viewref {:sub sub :referer referer :headers (str (:headers request))})
  (str (slurp "59.html")))

(defn record-blocked
  [db request referer]
  (insert! db :burl {:referer referer :headers (str (:headers request))})
  (str (slurp "59.html")))
