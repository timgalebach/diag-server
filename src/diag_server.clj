(ns diag-server
  (:require [com.stuartsierra.component :as component]
            [clojure.java.jdbc :as jdbc]
            [immutant.web :as web]
            [hikari-cp.core :refer [make-datasource close-datasource]]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [ring.middleware.params :refer [wrap-params]]
            [routes :as r]))

(defn wrap-db
  [db handler]
  (fn [request]
    (handler (assoc-in request [:params :db] db))))

(defrecord Server [port path params handler]
  component/Lifecycle
  (start [this]
    (println "Starting server on port:" port)
    (assoc this :params (web/run (:app handler) :port port)))
  (stop [this]
    (web/stop :port port)
    (assoc this :params nil :port nil :path nil :handler nil)))

(defrecord DBPool [conn fns username password server-name port-number database-name
                   adapter pool-size]
  component/Lifecycle
  (start [this]
    (println "Creating database pool; pool size:" pool-size)
    (try (assoc this :conn {:datasource
                             (make-datasource {:database-name database-name
                                               :adapter adapter
                                               :server-name server-name
                                               :password password
                                               :port-number port-number
                                               :username username
                                               :max-lifetime 1800000
                                               :minimum-idle 5
                                               :maximum-pool-size pool-size
                                               :connection-timeout 30000
                                               :auto-commit true
                                               :read-only false
                                               :idle-timeout 600000})})
         (catch Exception e (do (println "Postgres probably not started.")
                                (throw e)))))
  (stop [this]
    (if conn
      (do (println "Destroying database pool...")
          (close-datasource (:datasource conn))
          (assoc this :conn nil :fns nil)))
    this))


(defrecord Handler [app db config]
  component/Lifecycle
  (start [this]
    (assoc this :app (-> (wrap-db db app)
                         wrap-cookies
                         wrap-params)))
  (stop [this]
    (assoc this :db nil :app nil)))

(defn system [config-map]
  (let [{:keys [name db port] :as config} config-map]
    (println "Using profile:" name)
    (component/system-map
     :db        (map->DBPool db)
     :handler   (component/using
                 (map->Handler {:app (r/all) :config config})
                 [:db])
     :server    (component/using
                 (map->Server {:port port})
                 [:handler]))))
