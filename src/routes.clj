(ns routes
    (:require [compojure.core :refer [GET] :as cc]
              [compojure.route :as route]
              [handlers :as h]))

(defn all []
  (cc/routes (GET "/viewrequest" request
                  (str request))
             (GET "/viewref" request
                  (if-let [sub (get-in request [:params "s2"])]
                    (h/record-referer (get-in request [:params :db]) request
                                      (get (:headers request) "referer")
                                      (Long/parseLong sub))
                    (h/record-referer (get-in request [:params :db]) request
                                      (get (:headers request) "referer")
                                      0)))
             (GET "/submit/:campaign/:email" request
                  (str "success"))
             (GET "/burl" request
                  (h/record-blocked (get-in request [:params :db]) request
                                    (get (:headers request) "referer")))))
