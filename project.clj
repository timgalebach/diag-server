(defproject diag-server "0.1.0-SNAPSHOT"
  :description "server used for diagnostic purposes"
  :url "TODO"
  :license {:name "TODO: Choose a license"
            :url "http://choosealicense.com/"}
  :dependencies [[org.clojure/clojure "1.6.0"]]
  :profiles {:dev {:dependencies [[org.clojure/tools.namespace "0.2.7"]
                                  [com.stuartsierra/component "0.2.2"]
                                  [org.clojure/java.jdbc "0.3.5"]
                                  [org.postgresql/postgresql "9.2-1002-jdbc4"]
                                  [hikari-cp "0.13.0"]
                                  [clj-time "0.9.0"]
                                  [org.immutant/web "2.0.0-beta1"]
                                  [ring/ring-core "1.3.2"]
                                  [compojure "1.3.1"]]
                   :source-paths ["dev"]}})
